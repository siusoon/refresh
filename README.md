## &#35;refresh: Software Studies Manifesto

<img src="screenshots/amazon_refresh.png">
<img src="screenshots/weibo_refresh.png">
<img src="screenshots/google_refresh.png">
<img src="screenshots/Manifesto.gif">

The project is a browser extension, a piece of software art that intervenes web browsing by overlaying the Software Studies Manifesto.

This work is part of *[Software Studies, Revisited](http://computationalculture.net/software-studies-revisited/)*, the forthcoming article in Computational Culture Journal with Noah Wardrip-Fruin, Wendy Hui Kyong Chun and Jichen Zhu.

Feel free to fork, download, use and modify.

## How to run &#35;refresh

1. Fork or download this *refresh* directory, save the compressed file in your computer and unzip it (THe download button is next to the blue clone button on the top right area).
2. If you are using Firefox, go to step 3, else if you are using Chrome, go to step 4)
3. In the Firefox Address bar: Type `about:debugging`, then click "This Firefox" (in newer versions of Firefox), click "Load Temporary Add-on", then select the manifest.json file.
4. In the Chrome Address bar: Type `chrome://extensions`. Ensure the Developer mode is enable. Then click "Load unpacked" to select the refresh directory.
5. Browse any web pages as usual and you should able to see the pop-up manifesto (and you can close the pop-up by clicking the cross on the top right corner).

NB: No intention to put on any of the apps store

## Tested on:
- Linux Debian 10: Firefox ESR and Chrome
- Mac OS 10.14.16: Firefox and Chrome
- Mac OS 12.1: Firefox

## Reference

- Chun, Wendy Hui Kyong. *Programmed Visions: Software and Memory*. MIT Press, 2011.
- G-Cyrillus, "CSS rounded corners with gradient border [duplicate]," *stack overflow*, accessed August 3, 2021. https://stackoverflow.com/questions/68056814/css-rounded-corners-with-gradient-border.
