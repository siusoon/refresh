let refresh = `
<b> #refresh: Software Studies Manifesto (2022) </b></br></br>

Software Studies requires to hold down Ctrl while pressing R </br>
This is called refresh() </br>
The refresh() method refers to the arguments of multiplicities and pluralities </br>
The refresh() method expresses global, local, decolonized and queer variables </br></br>

Software Studies reloads its repository </br>
The repository coordinates multi-dimensional axes </br>
The repository pushes the boundaries of normativity </br>
The repository merges malleable practices </br></br>

Software Studies declares software as a site of societal and technical power </br>
this.site recognizes the discrepancy of What You See Is NOT What You Get </br>
this.site questions the (in)visibility of systems </br>
this.site inits new repositories
`;

document.body.insertAdjacentHTML( 'afterbegin', '<div id="stage"></div><div id="manifest"><span id="perform">X</span>'+refresh+'</div>')
document.getElementById("manifest").style.cssText = `
  display: block; position: fixed; z-index: 199;
  animation: skew 6s infinite; transform: skew(20deg); animation-direction: alternate;
  width: 750px; height: 380px;
  top: 20%; left: 20%;
  margin: 0 auto; padding: 20px 0;
  font-size: 16px; color:#FFFFF0;
  line-height:normal; text-align: center;
  border: 4px solid transparent; border-radius: 6rem 1rem;
  background:
    linear-gradient(to right, rgba(0,0,0,0.9), rgba(0,0,0,0.96)),
    linear-gradient(to bottom right, #b827fc 0%, #2c90fc 25%, #b8fd33 50%, #fec837 75%, #fd1892 100%);
    background-clip: padding-box, border-box; background-origin: padding-box, border-box;
`;
let makeChange = document.createElement("style");
makeChange.innerText = `
  @keyframes skew {
    0% {transform: skew(30deg, 20deg);}
    100% {transform: skew(0deg, 0deg);}
  }
`;
document.head.appendChild(makeChange);
document.getElementById("stage").style.cssText = `
  display: block; position: absolute; z-index: 198;
  top: 0; left: 0;
  height: 100%; width: 100%;
  background-color: rgba(0, 0, 0, 0.6);
`;
document.getElementById("perform").style.cssText = `
  float: right; padding-right: 10px; padding-top: -18px;
  cursor: pointer;
`;
document.getElementById("perform").onclick = function() {
  document.getElementById("stage").style.display = 'none';
  document.getElementById("manifest").style.display = 'none';
};
